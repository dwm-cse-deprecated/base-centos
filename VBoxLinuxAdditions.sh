#!/bin/bash

set -x

mkdir /tmp/iso
mount -t iso9660 -o loop /dev/cdrom1 /tmp/iso
sh /tmp/iso/VBoxLinuxAdditions.run
umount /tmp/iso
rmdir /tmp/iso
