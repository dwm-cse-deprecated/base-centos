#!/bin/bash

set -ex

#########################################################
# base configuration                                    #
# - target os: ceontos6                                 #
# - target platform: virtualbox, docker, amazon ec2     #
# - target env: local and other (dev,test,prod)         #
#########################################################

packer_build_name=$1
packer_build_env=$2
basedir=`cd $(dirname ${0}) && pwd`

# keep yum packages
sed -i -e "s/keepcache=0/keepcache=1/g" /etc/yum.conf

yum groupinstall -y 'Base' 'Development Libraries' 'Development Tools' 'Editors' 'Legacy Software Development'
yum update -y

# CentOS apacheのログローテとを4時にする
yum install -y cronie-noanacron
yum remove -y cronie-anacron

# bashrc
cp ${basedir}/configs/etc/bashrc /etc/bashrc
chmod 644 /etc/bashrc
chown root.root /etc/bashrc

# su (wheel)
cp ${basedir}/configs/etc/pam.d/su /etc/pam.d/su
chmod 644 /etc/pam.d/su
chown root.root /etc/pam.d/su

# sshd_config
cp ${basedir}/configs/etc/ssh/sshd_config /etc/ssh/sshd_config
chmod 600 /etc/ssh/sshd_config
chown root.root /etc/ssh/sshd_config

# selinux: disabled
cp ${basedir}/configs/etc/selinux/config /etc/selinux/config
chmod 644 /etc/selinux/config
chown root.root /etc/selinux/config

# ipv6: disabled
cp ${basedir}/configs/etc/modprobe.d/disable-ipv6.conf /etc/modprobe.d/disable-ipv6.conf
chmod 644 /etc/modprobe.d/disable-ipv6.conf
chown root.root /etc/modprobe.d/disable-ipv6.conf

# pkgs
yum -y install make
yum -y install gcc
yum -y install wget
yum -y install unzip
yum -y install bind-utils
yum -y install bind-libs
yum -y install sysstat
yum -y install rsync
yum -y install mailx
yum -y install tmpwatch
yum -y install tcpdump
yum -y install dstat
yum -y install telnet
yum -y install git

# rsyslogd
yum -y install rsyslog
chkconfig rsyslog on

# snmpd
yum -y install net-snmp-utils
yum -y install net-snmp
chkconfig snmpd on

cp ${basedir}/configs/etc/snmp/snmpd.conf /etc/snmp/snmpd.conf
chmod 644 /etc/snmp/snmpd.conf
chown root.root /etc/snmp/snmpd.conf

# postfix
yum -y install postfix
chkconfig postfix on

cp ${basedir}/configs/etc/postfix/main.cf /etc/postfix/main.cf
chmod 644 /etc/postfix/main.cf
chown root.root /etc/postfix/main.cf

# ntpd
yum -y install ntp
chkconfig ntpd on

cp ${basedir}/configs/etc/sysconfig/ntpd /etc/sysconfig/ntpd
chmod 644 /etc/sysconfig/ntpd
chown root.root /etc/sysconfig/ntpd

# krb5
yum -y install pam_krb5

cp ${basedir}/configs/etc/krb5.conf /etc/krb5.conf
chmod 644 /etc/krb5.conf
chown root.root /etc/krb5.conf

cp ${basedir}/configs/etc/pam.d/password-auth-ac /etc/pam.d/password-auth-ac
chmod 644 /etc/pam.d/password-auth-ac
chown root.root /etc/pam.d/password-auth-ac

cp ${basedir}/configs/etc/pam.d/sshd /etc/pam.d/sshd
chmod 644 /etc/pam.d/sshd
chown root.root /etc/pam.d/sshd

# useradd: ati
useradd ati -u 733
cp -r ${basedir}/configs/home/ati/.ssh /home/ati/.ssh
chown ati.ati -R /home/ati/.ssh/
chmod 700 /home/ati/.ssh/
chmod 600 /home/ati/.ssh/authorized_keys
  
cp ${basedir}/configs/etc/sudoers.d/ati /etc/sudoers.d/ati
chmod 440 /etc/sudoers.d/ati
chown root.root /etc/sudoers.d/ati

# useradd: rundeck
useradd rundeck -u 734
cp -r ${basedir}/configs/home/rundeck/.ssh /home/rundeck/.ssh
chown rundeck.rundeck -R /home/rundeck/.ssh/
chmod 700 /home/rundeck/.ssh/
chmod 600 /home/rundeck/.ssh/authorized_keys

cp ${basedir}/configs/etc/sudoers.d/rundeck /etc/sudoers.d/rundeck
chmod 440 /etc/sudoers.d/rundeck
chown root.root /etc/sudoers.d/rundeck

# useradd: enmob
useradd enmob -u 8802
###usermod -aG wheel enmob
cp -r ${basedir}/configs/home/enmob/.ssh /home/enmob/.ssh
chown enmob.enmob -R /home/enmob/.ssh/
chmod 700 /home/enmob/.ssh/
chmod 600 /home/enmob/.ssh/authorized_keys

cp ${basedir}/configs/etc/sudoers.d/enmob /etc/sudoers.d/enmob
chmod 440 /etc/sudoers.d/enmob
chown root.root /etc/sudoers.d/enmob

# abash
cd /usr/local/src
curl -O http://dev-chef.in.dwango.jp/abash-4.1.2-15.4ML6.el6.x86_64.rpm
rpm -ivh abash-4.1.2-15.4ML6.el6.x86_64.rpm
cd $basedir

chsh -s /bin/abash root
if [ "$packer_build_name" = "amazon-ebs" ]; then
chsh -s /bin/abash centos
fi

cp ${basedir}/configs/etc/default/useradd /etc/default/useradd
chmod 600 /etc/default/useradd
chown root.root /etc/default/useradd

cp ${basedir}/configs/etc/rsyslog.conf /etc/rsyslog.conf
chmod 644 /etc/rsyslog.conf
chown root.root /etc/rsyslog.conf

cp ${basedir}/configs/etc/logrotate.d/syslog /etc/logrotate.d/syslog
chmod 644 /etc/logrotate.d/syslog
chown root.root /etc/logrotate.d/syslog

# zabbix
cd /usr/local/src
curl -O http://dev-chef.in.dwango.jp/zabbix-2.2.8-1.el6.x86_64.rpm
curl -O http://dev-chef.in.dwango.jp/zabbix-agent-2.2.8-1.el6.x86_64.rpm
curl -O http://dev-chef.in.dwango.jp/zabbix-get-2.2.8-1.el6.x86_64.rpm
curl -O http://dev-chef.in.dwango.jp/zabbix-sender-2.2.8-1.el6.x86_64.rpm
rpm -Uvh zabbix*-2.2.8-1.el6.x86_64.rpm
chkconfig zabbix-agent on
cd $basedir

cp ${basedir}/configs/etc/zabbix/zabbix_agentd.conf /etc/zabbix/zabbix_agentd.conf
chmod 644 /etc/zabbix/zabbix_agentd.conf
chown root.root /etc/zabbix/zabbix_agentd.conf

cp ${basedir}/configs/etc/zabbix/zabbix_agentd.d/userparameter_os.conf /etc/zabbix/zabbix_agentd.d/userparameter_os.conf
chmod 644 /etc/zabbix/zabbix_agentd.d/userparameter_os.conf
chown root.root /etc/zabbix/zabbix_agentd.d/userparameter_os.conf 

chmod 644 /var/log/messages*
chmod 0755 /etc/init.d/zabbix-agent

# disable unnecessary services
chkconfig blk-availability off
chkconfig ip6tables off
chkconfig iptables off
chkconfig iscsi off
chkconfig iscsid off
chkconfig lvm2-monitor off
chkconfig mdmonitor off
chkconfig netfs off

echo "Conguraturations!!! The Base Provisioner successfully completed."